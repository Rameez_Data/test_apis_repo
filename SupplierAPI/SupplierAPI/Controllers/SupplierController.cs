﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupplierAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupplierAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/SupplierAPI")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly SupplierContext _context; 
        
        public SupplierController(SupplierContext context)
        {
            _context = context;

            if (_context.Suppliers.Count() == 0)
            {
                _context.Suppliers.Add(new Supplier
                {
                    CompanyName = "Fahad Traders",
                    ContactName = "Peter Wilson",
                    City = "Rawalpindi",
                    Country = "Pakistan",
                    Phone = "(0332)555-3267",
                    ContactTitle = "Manager",
                    Fax = "345345"
                });

                _context.Suppliers.Add(new Supplier
                {
                    CompanyName = "Exotic Liquids",
                    ContactName = "Peter Wilson",
                    City = "Boston",
                    Country = "USA",
                    Phone = "(0332)555-3267",
                    ContactTitle = "Manager",
                    Fax = "67867867868"
                });
                _context.SaveChanges();
            }
        }
        
        [HttpGet]
        public IEnumerable GetAll()
        {
            return _context.Suppliers.ToList();
        }

        [HttpGet("{id}", Name = "GetSupplier")]
        public IActionResult GetById(long id)
        {
            var item = _context.Suppliers.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Supplier item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _context.Suppliers.Add(item);
            _context.SaveChanges();

            return new NoContentResult();
            //return CreatedAtRoute("GetById", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Supplier item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            var supplier = _context.Suppliers.FirstOrDefault(t => t.Id == id);
            if (supplier == null)
            {
                return NotFound();
            }

            supplier.CompanyName = item.CompanyName;
            supplier.ContactName = item.ContactName;
            supplier.ContactTitle = item.ContactTitle;
            supplier.Country = item.Country;
            supplier.City = item.City;
            supplier.Phone = item.Phone;
            supplier.Fax = item.Fax;

            _context.Suppliers.Update(supplier);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var supplier = _context.Suppliers.FirstOrDefault(t => t.Id == id);
            if (supplier == null)
            {
                return NotFound();
            }

            _context.Suppliers.Remove(supplier);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
