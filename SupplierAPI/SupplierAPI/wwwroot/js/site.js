﻿const uri = 'api/SupplierAPI';
let suppliers = [];
function getItems() {
    fetch(uri)
        .then(response => response.json())
        .then(data => _displayItems(data))
        .catch(error => console.error('Unable to get items.', error));
}

function addItem() {
    const addCompanyTextbox = document.getElementById('add-Company');
    const addNameTextbox = document.getElementById('add-Name');
    const addTitleTextbox = document.getElementById('add-Title');
    const addCityTextbox = document.getElementById('add-city');
    const addCountryTextbox = document.getElementById('add-country');
    const addPhoneTextbox = document.getElementById('add-phone');
    const addFaxTextbox = document.getElementById('add-fax');

    const item = {
        CompanyName: addCompanyTextbox.value.trim(),
        ContactName: addNameTextbox.value.trim(),
        ContactTitle: addTitleTextbox.value.trim(),
        city: addCityTextbox.value.trim(),
        country: addCountryTextbox.value.trim(),
        phone: addPhoneTextbox.value.trim(),
        fax: addFaxTextbox.value.trim()
    };

    fetch(uri, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
        .then(() => {
            getItems();
            addCompanyTextbox.value = '';
            addNameTextbox.value = '';
            addTitleTextbox.value = '';
            addCityTextbox.value = '';
            addCountryTextbox.value = '';
            addPhoneTextbox.value = '';
            addFaxTextbox.value = '';
        })
        .catch(error => console.error('Unable to add item.', error));
}

function deleteItem(id) {
    fetch(`${uri}/${id}`, {
        method: 'DELETE'
    })
        .then(() => getItems())
        .catch(error => console.error('Unable to delete item.', error));
}

function displayEditForm(id) {
    const item = suppliers.find(item => item.id === id);
    document.getElementById('edit-Company').value = item.companyName;
    document.getElementById('edit-Name').value = item.contactName;
    document.getElementById('edit-Title').value = item.contactTitle;
    document.getElementById('edit-city').value = item.city;
    document.getElementById('edit-country').value = item.country;
    document.getElementById('edit-phone').value = item.phone;
    document.getElementById('edit-fax').value = item.fax;
    document.getElementById('edit-id').value = item.id;
    document.getElementById('editForm').style.display = 'block';
}

function updateItem() {
    const itemId = document.getElementById('edit-id').value;
    const item = {
        id: parseInt(itemId, 10),
        CompanyName: document.getElementById('edit-Company').value.trim(),
        ContactName: document.getElementById('edit-Name').value.trim(),
        ContactTitle: document.getElementById('edit-Title').value.trim(),
        city: document.getElementById('edit-city').value.trim(),
        country: document.getElementById('edit-country').value.trim(),
        phone: document.getElementById('edit-phone').value.trim(),
        fax: document.getElementById('edit-fax').value.trim()
    };

    fetch(`${uri}/${itemId}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
        .then(() => getItems())
        .catch(error => console.error('Unable to update item.', error));

    closeInput();

    return false;
}

function closeInput() {
    document.getElementById('editForm').style.display = 'none';
}

function _displayCount(itemCount) {
    const name = (itemCount === 1) ? 'supplier' : 'suppliers';

    document.getElementById('counter').innerText = `${itemCount} ${name}`;
}

function _displayItems(data) {
    const tBody = document.getElementById('supplier');
    tBody.innerHTML = '';

    _displayCount(data.length);

    const button = document.createElement('button');

    data.forEach(item => {

        let editButton = button.cloneNode(false);
        editButton.innerText = 'Edit';
        editButton.setAttribute('onclick', `displayEditForm(${item.id})`);

        let deleteButton = button.cloneNode(false);
        deleteButton.innerText = 'Delete';
        deleteButton.setAttribute('onclick', `deleteItem(${item.id})`);

        let tr = tBody.insertRow();

        let td1 = tr.insertCell(0);
        let textNode = document.createTextNode(item.companyName);
        td1.appendChild(textNode);

        let td2 = tr.insertCell(1);
        let textNode2 = document.createTextNode(item.contactName);
        td2.appendChild(textNode2);

        let td3 = tr.insertCell(2);
        let textNode3 = document.createTextNode(item.contactTitle);
        td3.appendChild(textNode3);

        let td4 = tr.insertCell(3);
        let textNode4 = document.createTextNode(item.city);
        td4.appendChild(textNode4);

        let td5 = tr.insertCell(4);
        let textNode5 = document.createTextNode(item.country);
        td5.appendChild(textNode5);

        let td6 = tr.insertCell(5);
        let textNode6 = document.createTextNode(item.phone);
        td6.appendChild(textNode6);

        let td7 = tr.insertCell(6);
        let textNode7 = document.createTextNode(item.fax);
        td7.appendChild(textNode7);

        let td8 = tr.insertCell(7);
        td8.appendChild(editButton);

        let td9 = tr.insertCell(8);
        td9.appendChild(deleteButton);
    });

    suppliers = data;
}